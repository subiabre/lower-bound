<?php

namespace Subiabre;

/**
 * PHP implementation of the Lower bound of Wilson \
 * As described by Evan Miller
 * @package subiabre/lower-bound
 * @author subiabrewd@gmail.com
 * @license MIT
 * @see http://www.evanmiller.org/how-not-to-sort-by-average-rating.html
 */
class LowerBound
{
    /**
     * Default confidence value: `0.95`
     */
    const CONFIDENCE_DEFAULT = 0.95;

    protected 
        $score,
        $confidence,
        $quantile,
        $positive,
        $negative,
        $total,
        $p;

    public function __construct()
    {

        $this->confidence = self::CONFIDENCE_DEFAULT;
    }

    /**
     * Set the confidence value, 0.95 by default
     * @param float $confidence Percentage as a float from 0 to 1
     * @return self
     */
    public function setConfidence(float $confidence = self::CONFIDENCE_DEFAULT): self
    {
        if ($confidence > 1)
        {
            throw new \Exception("Param: confidence must be a value between 0 and 1");
        }

        $this->confidence = $confidence;
        $this->setQuantile($confidence);

        return $this;
    }

    /**
     * Obtain the confidence value
     * @return float Confidence percentage as a float from 0 to 1
     */
    public function getConfidence(): float
    {
        return $this->confidence;
    }

    /**
     * Calculate a new quantile with the given confidence
     * @param float $confidence Percentage of confidence as a float from 0 to 1
     */
    public function setQuantile(float $confidence = self::CONFIDENCE_DEFAULT): self
    {
        $this->quantile = $this->pnorm(1 - (1-$confidence) / 2);

        return $this;
    }

    /**
     * Obtain the quantile value
     * @return float Quantile calculated value from confidence
     */
    public function getQuantile(): float
    {
        return $this->quantile;
    }

    /**
     * Pass ratings
     * @param int $positive Total number of positive ratings
     * @param int $negative Total number of negative ratings
     * @return self
     */
    public function addRatings(int $positive, int $negative): self
    {
        $this->positive = $positive;
        $this->negative = $negative;
        $this->total = $positive + $negative;

        if ($this->total > 0) $this->p = $positive / $this->total;

        return $this;
    }
    
    /**
     * Obtain the ratings totals
     * @return array
     */
    public function getRatings(): array
    {
        return [
            'positive' => $this->positive,
            'negative' => $this->negative,
            'total' => $this->total
        ];
    }

    /**
     * Get lower bound score
     * @return float
     */
    public function getLowerBound(): float
    {
        if ($this->total < 1) return 0;
        return $this->multiply() * ($this->innerLeft() - $this->innerRight());
    }

    /**
     * Get upper bound score
     * @return float
     */
    public function getUpperBound(): float
    {
        if ($this->total < 1) return 0;
        return $this->multiply() * ($this->innerLeft() + $this->innerRight()); 
    }


    /**
     * From ruby Statistics2.pnorm
     * @param $quantile
     * @return float
     * @see https://github.com/abscondment/statistics2/blob/master/lib/statistics2/base.rb#L89
     */
    protected function pnorm($quantile): float
    {
        $b = [
            1.570796288, 
            0.03706987906, 
            -0.8364353589e-3, 
            -0.2250947176e-3,
            0.6841218299e-5, 
            0.5824238515e-5, 
            -0.104527497e-5, 
            0.8360937017e-7,
            -0.3231081277e-8, 
            0.3657763036e-10, 
            0.6936233982e-12
        ];

        if ($quantile < 0.0 || $quantile > 1.0 || $quantile == 0.5) return 0.0;

        $w1 = $quantile > 0.5 ? 1.0 - $quantile : $quantile;
        $w3 = - \log(4.0 * $w1 * (1.0 - $w1));
        $w1 = $b[0];

        for ($i = 1; $i <= 10; $i++) {
            $w1 += $b[$i] * \pow($w3, $i);
        }
        
        if ($quantile > 0.5)
        {
            return \sqrt($w1 * $w3);
        }

        return -\sqrt($w1 * $w3);
    }

    /**
     * @return float
     */
    private function multiply(): float
    { 
        return 1 / (1 + (\pow($this->quantile, 2) / $this->total)); 
    }

    /**
     * @return float
     */
    private function innerLeft(): float
    {
        return $this->p + (\pow($this->quantile, 2) / (2 * $this->total)); 
    }
    /**
     * @return float
     */
    private function innerRight(): float
    {
        return $this->quantile * \sqrt(($this->p * (1 - $this->p) / $this->total) + (\pow($this->quantile, 2) / (4 * \pow($this->total, 2)))); 
    }
}
