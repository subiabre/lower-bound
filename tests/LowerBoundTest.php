<?php

namespace Subiabre\Tests;

use Subiabre\LowerBound;
use PHPUnit\Framework\TestCase;

class LowerBoundTest extends TestCase
{
    private $score;

    public function setUp(): void
    {
        $this->score = new LowerBound();
    }

    public function testSetsConfidence()
    {
        $score = $this->score->setConfidence(0.5);
        $confidence = $this->score->getConfidence();

        $this->assertInstanceOf(LowerBound::class, $score);

        $this->assertEquals(
            0.5,
            $confidence
        );
    }

    public function testSetConfidenceThrowsException() {
        try {
            $this->score->setConfidence(1.1);
            $this->expectException(\Exception::class);
        } catch (\Exception $e) {
            $this->assertIsObject($e);
        }
    }

    public function testSetQuantileCalsQuantile()
    {
        $this->score->setQuantile(0.95);
        $quantile = $this->score->getQuantile();

        $this->assertEquals(
            1.9599639715843482,
            $quantile
        );
    }

    public function testAddsRatings()
    {
        $score = $this->score->addRatings(10, 2);
        $ratings = $this->score->getRatings();

        $this->assertInstanceOf(LowerBound::class, $score);

        $this->assertEquals(
            10,
            $ratings['positive']
        );

        $this->assertEquals(
            2,
            $ratings['negative']
        );

        $this->assertEquals(
            12,
            $ratings['total']
        );
    }

    public function testLowerBound() {
        $this->score->addRatings(31, 6);
        $lowerBound = $this->score->getLowerBound();

        $this->assertIsFloat($lowerBound);
    }

    public function testUpperBound() {
        $this->score->addRatings(31, 6);
        $upperBound = $this->score->getUpperBound();

        $this->assertIsFloat($upperBound);
    }

    public function testBounds() {
        $this->score->addRatings(31, 6);
        $upperBound = $this->score->getUpperBound();
        $lowerBound = $this->score->getLowerBound();

        $this->assertGreaterThanOrEqual($upperBound, $lowerBound);
    }

}
